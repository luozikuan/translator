#include <QMessageBox>
#include <QFileDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionAbout_Qt, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt()));
    connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(openFile()));
    connect(ui->actionQuit, SIGNAL(triggered(bool)), this, SLOT(close()));
    connect(ui->actionAbout_translate, SIGNAL(triggered(bool)), this, SLOT(aboutThis()));

    ui->comboBox->addItem("English");
    ui->comboBox->addItem(QStringLiteral("中文"));
    ui->comboBox->addItem(QStringLiteral("日本語"));

    translator = new QTranslator(this);
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(changeLanguage(int)));

    QLocale locale = QLocale::system();
    if (locale.language() == QLocale::Chinese)
        ui->comboBox->setCurrentIndex(1);
    else if (locale.language() == QLocale::Japanese)
        ui->comboBox->setCurrentIndex(2);
    else
        ui->comboBox->setCurrentIndex(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::aboutThis()
{
    QMessageBox::information(this, tr("About this"), tr("This is a translator test writen by LUO ZiKuan."));
}

void MainWindow::openFile()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("Open"),
                                                    "/Users/luozikuan/Pictures",
                                                    tr("Images (*.png *.xpm *.jpg)"));
    if (!filename.isEmpty())
        QMessageBox::information(this, tr("Info"), tr("You have select %1").arg(filename));
    else
        QMessageBox::warning(this, tr("Warning"), tr("You didn't select any file!"));
}

void MainWindow::changeLanguage(int index)
{
    QString langDir = "/Users/luozikuan/Developer/qt/translate/";
    switch (index)
    {
    case 1:
        translator->load(langDir + "lang_zh-CN");
        qApp->installTranslator(translator);
        break;
    case 2:
        translator->load(langDir + "lang_jp");
        qApp->installTranslator(translator);
        break;
    default:
        QMessageBox::warning(this, tr("Warning"), tr("translate file is missing!"));
    case 0:
        qApp->removeTranslator(translator);
        break;
    }
    ui->retranslateUi(this);
}

void MainWindow::on_pushButton_clicked()
{
    QMessageBox::information(this, tr("Info"), tr("You have clicked the test button."));
}
