#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void openFile();
    void changeLanguage(int index);
    void aboutThis();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    QTranslator *translator;
};

#endif // MAINWINDOW_H
