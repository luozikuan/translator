<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>主窗口</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>click me</source>
        <translation>点我</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40"/>
        <source>please select language</source>
        <translation>请选择语言</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <source>&amp;File</source>
        <translation>文件(&amp;F)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <source>About Qt</source>
        <translation>关于QT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="92"/>
        <source>About this app</source>
        <oldsource>About translate</oldsource>
        <translation>关于此程序</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <location filename="mainwindow.cpp" line="46"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="102"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="40"/>
        <source>About this</source>
        <translation>关于此程序</translation>
    </message>
    <message>
        <source>This is a translator test writen by LUO ZiKuan</source>
        <translation type="vanished">这是罗滋宽写的翻译测试程序。</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="40"/>
        <source>This is a translator test writen by LUO ZiKuan.</source>
        <translation>这是罗滋宽写的翻译测试程序。</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <source>Images (*.png *.xpm *.jpg)</source>
        <translation>图片 (*.png *.xpm *.jpg)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="50"/>
        <location filename="mainwindow.cpp" line="79"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="50"/>
        <source>You have select %1</source>
        <translation>你选择了 %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="52"/>
        <location filename="mainwindow.cpp" line="69"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="52"/>
        <source>You didn&apos;t select any file!</source>
        <translation>你没有选择任何文件！</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="69"/>
        <source>translate file is missing!</source>
        <translation>语言包丢失！</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="79"/>
        <source>You have clicked the test button.</source>
        <translation>你点击了测试按钮。</translation>
    </message>
</context>
</TS>
