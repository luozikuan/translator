<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>メインウインドウ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="27"/>
        <source>click me</source>
        <translation>私をクリックします</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="40"/>
        <source>please select language</source>
        <translation>言語を選択してください</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <source>&amp;File</source>
        <translation>ファイル(&amp;F)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <source>About</source>
        <translation>およそ</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <source>About Qt</source>
        <translation>およそ QT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="92"/>
        <source>About this app</source>
        <translation>このアプリケーションについて</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <location filename="mainwindow.cpp" line="46"/>
        <source>Open</source>
        <translation>オープン</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="102"/>
        <source>Quit</source>
        <translation>やめます</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="40"/>
        <source>About this</source>
        <translation>これについて</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="40"/>
        <source>This is a translator test writen by LUO ZiKuan.</source>
        <translation>これは罗滋宽によって書か翻訳者試験です。</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="48"/>
        <source>Images (*.png *.xpm *.jpg)</source>
        <translation>イメージ (*.png *.xpm *.jpg)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="50"/>
        <location filename="mainwindow.cpp" line="79"/>
        <source>Info</source>
        <translation>情報</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="50"/>
        <source>You have select %1</source>
        <translation>あなたは %1 を選択しました</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="52"/>
        <location filename="mainwindow.cpp" line="69"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="52"/>
        <source>You didn&apos;t select any file!</source>
        <translation>あなたは、任意のファイルを選択しませんでした！</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="69"/>
        <source>translate file is missing!</source>
        <translation>ファイルが存在しない翻訳！</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="79"/>
        <source>You have clicked the test button.</source>
        <translation>あなたは、テストボタンをクリックしました。</translation>
    </message>
</context>
</TS>
